import './App.css';
import { useEffect, useState } from 'react';
import Card from './components/Card';

function App() {

  const [attendeeList, setAttendeeList] = useState([])
  const [addInvite, setAddInvite] = useState('');
  const [editIndex, setEditIndex] = useState(-1);
  const [editName, setEditName] = useState('');
  const [hideUnconfirmed, setHideUnconfirmed] = useState(false)


  useEffect(() => {
    console.log("Use Effect")
  }, [attendeeList, editName, editIndex, handleConfirmed, hideUnconfirmed])

  function handleSubmit(e) {
    e.preventDefault();
    if (addInvite.name === '') {

      return
    }
    setAttendeeList([...attendeeList, addInvite])
    setAddInvite({ name: '', confirmed: false })
    //console.log(attendeeList)
  }
  function handleEdit(id) {
    console.log(id)
    setEditIndex(id);
    console.log("Edit Name")
    console.log(editName)
    setEditName(attendeeList[id])
    //console.log(editIndex)
    console.log(attendeeList[id])
    console.log(editName)

  }

  function handleSave(id) {
    console.log("Save")
    const newList = [...attendeeList];
    newList[id] = editName;
    setAttendeeList(newList);
    setEditIndex(-1);
    setEditName('')

    //console.log("save")
  }

  function handleDelete(id) {
    const newList = [...attendeeList];
    newList.splice(id, 1);
    setAttendeeList(newList)
  }

  function switchUnconfirmed() {
    setHideUnconfirmed(!hideUnconfirmed)
    if (hideUnconfirmed === true) {
      //console.log("HIDE Unwanted")
    }

    if (hideUnconfirmed === false) {
      //console.log("Show All")
    }





    console.log("Switched to: ")
    console.log(hideUnconfirmed)
  }

  function handleConfirmed(id) {
    const newList = [...attendeeList]
    newList[id].confirmed = !newList[id].confirmed
    setAttendeeList(newList)
  }
  return (
    <div className="wrapper">
      <header>
        <h1>RSVP</h1>
        <p>A Mycode App</p>
        <section id="registrar">
          <form onSubmit={handleSubmit} >
            <div className='flex flex-row  min-width '>
              <div className='flex justify-end'>
                <input type="text" name="name" placeholder="Invite Someone" className="person min-width" value={addInvite.name} onChange={(e) => { setAddInvite({ name: e.target.value, confirmed: false }) }} />
              </div>
              <div className='flex min-width justify-end'>
                <button className="submit w-4">Submit</button>
              </div>
            </div>
          </form>
        </section>
      </header>

      <div className="main">
        <h2>Invitees</h2>
        <div>
          <label>Hide those who haven't responded</label>
          <input className="confirmed"
            type="checkbox"
            onChange={() => { switchUnconfirmed() }} />
        </div>

        <Card
          handleDelete={handleDelete}
          handleSave={handleSave}
          handleSubmit={handleSubmit}
          handleEdit={handleEdit}
          handleConfirmed={handleConfirmed}
          attendeeList={attendeeList}
          editIndex={editIndex}
          setEditName={setEditName}
          editName={editName}
          hideUnconfirmed={hideUnconfirmed}
        ></Card>


      </div>

    </div>
  );
}

export default App;
