import React from "react";


function Card(props) {
    return (<>
            <ul id="invitedList">

{props.attendeeList.map((item, id) => {

    if(props.hideUnconfirmed===true && !item.confirmed){
        
      return
    }
    return <li key={id}>
        {id === props.editIndex ?
            (
                <>
                    <span>
                        <input type="text" value={props.editName.name} onChange={(e) => props.setEditName({name:e.target.value,confirmed:item.confirmed})}>
                        </input>
                    </span>
                    <label>
                        Confirmed
                        <input type='checkbox' checked={item.confirmed} onChange={()=>{props.handleConfirmed(id)}}/>
                    </label>
                    <button onClick={() => { props.handleSave(id) }}>
                        Save
                    </button>
                </>
            ) : (
                <>
                    <span>
                        {item.name}
                    </span>
                    <label>
                        Confirmed
                        <input type="checkbox" checked={item.confirmed} onChange={()=>{props.handleConfirmed(id)}}/>
                    </label>
                    <button onClick={() => props.handleEdit(id)}>
                        edit
                    </button>
                </>)
        }<button onClick={() => { props.handleDelete(id) }}>remove</button></li>
})}
</ul>
    </>
    )
}

export default Card;