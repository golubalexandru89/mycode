import logo from './logo.svg';
import './App.css';
import {useState} from "react";
import Counter from './components/counter';
import SecondCounter from './components/counter2'
import DoubleLinked from './components/doubleLinked';
import ChildCounter from './components/childCounter';

export default function App() {
  const [counter,setCounter]=useState(0);
  
  return (
    <div>
      <p> Counter is at {counter}</p>
   <ChildCounter alex={counter}></ChildCounter>
   <button onClick={(event)=>{
    setCounter(counter+1)
   }}> button</button>
   </div>
  )
}
