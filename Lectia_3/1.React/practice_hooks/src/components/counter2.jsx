import React, { useState, useEffect} from "react";

function SecondCounter(){
    const [counter, handleClick] = useState(0);

    useEffect(() => {
        document.title = `You clicked ${counter} times`;
      });

    return (
        <div>
          <button onClick={() => ( handleClick(counter + 1)) }>Increment counter</button>
          {/* <button onClick={this.handleClick}>Increment counter</button> */}
          <div>Counter value is {counter}</div>
        </div>
      );

}

export default SecondCounter;