import { useState, useEffect } from "react";
export default function Counter() {

  let [flower, setFlower] = useState("red");
  let [persona, setPersona] = useState('man')

  let changePersona = () => {
    if (persona == "man") {
      setPersona("woman");
    } else if (persona == "woman") {
      setPersona("man")
    }
  }

  let changeColor = () => {
    flower == "red" ? setFlower("blue") : setFlower("red")
  }



  return (
    <>
      <div>
        <h1 className="text-3xl font-bold underline">
          Hello world!
        </h1>
        <p> The {persona} smelled a {flower} rose.</p>
        <button onClick={changePersona}>Change to {persona}</button>
        <br></br>
        <button onClick={changeColor}>Change color to {flower}</button>

      </div>
    </>
  )
}