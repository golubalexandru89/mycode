import { useEffect, useState } from "react";
import Card from "./Card";

import {getAllCustomers} from '../../service/api.js'
import { useNavigate } from "react-router-dom";
 function Home() {

    let [customers,setCustomers]=useState([]);

    let navigate=useNavigate();


    let handleCustomers=async ()=>{
    
        let data=await getAllCustomers();
        //let data = [{id:1,email:"alex@alex.ro",password:"123",fullName:"Alex G",orders:[]},{id:2,email:"anca@anca.ro",password:"123",fullName:"Anca D",orders:[]},{id:3,email:"mihai@mihai.ro",password:"123",fullName:"Mihai B",orders:[]},{id:4,email:"florin@florin.ro",password:"123",fullName:"Florin V",orders:[]},{id:5,email:"viorel@viorel.ro",password:"123",fullName:"Viorel B",orders:[]},{id:6,email:"iulia@iulia.ro",password:"123",fullName:"Iulia E",orders:[]}]

         setCustomers(data);
    }

    useEffect(()=>{
        handleCustomers();
        
    },[]);

    function handleAdd() {
        navigate("/add");
    }

    return (
        <>
                <h1>Customers</h1>
                <p><a className="button" onClick={handleAdd}>Create Customer</a></p>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                       {

                        customers.length>0&&(



                              customers.map(cu=>{

                                 return <Card customer={cu} key={cu.id}/>
                              })
                        )
                          
                       }

                       {

                         customers.length==0&&(

                             <tr><td>Loading.....</td></tr>
                            
                         )

                       }

                    </tbody>
                </table>
        </>
    )
}

export default Home;