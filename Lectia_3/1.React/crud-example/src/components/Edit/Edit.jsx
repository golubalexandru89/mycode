import { useEffect, useState } from "react";
import deleteEmail, { getAllCustomers } from "../../service/api";
import { useNavigate, useParams } from "react-router-dom";

function Edit() {

    const navigate = useNavigate();

    function cancelEdit() {
        navigate("/")
    }

    const [user, setUser] = useState({});

    let { custId } = useParams();


    useEffect(() => {
        retrieveCustomer();
    }, []);


    async function retrieveCustomer() {

        //console.log("asdas");
        let customerList = await getAllCustomers();
        console.log(custId)
        let user = customerList.filter((cs) => cs.id == custId)
        let userFound = user[0]
        //console.log(userFound)
        setUser(userFound)

    }

    async function deleteCustomer(){
        let customer = await deleteEmail(user.email)
        navigate("/")
    }    
    //console.log(user.fullName)
    return (
        <>
            <h1>Update Customer</h1>
            <section className="app-form">
                <p>
                    <label htmlFor="name">Name</label>
                    <input name="name" type="text" id="name" defaultValue={user.fullName} />
                </p>
                <p>
                    <label htmlFor="email">Email</label>
                    <input name="email" type="text" id="email" defaultValue={user.email} />
                </p>
                <p>
                    <label htmlFor="password">Password</label>
                    <input name="password" type="text" id="password" defaultValue={user.password} />
                </p>


            </section>
            <section className="app-form flex-row">
                <p><a className="button "> Submit</a></p>
            
                <p> <a className="button" onClick={cancelEdit}>Cancel</a></p>
            
                <p><a className="button" onClick={deleteCustomer}>Delete </a></p>
            </section>
        </>
        //  <Link to={`/edit/${item.id}`}>{item.name}</Link>
    )
}

export default Edit;