function api(path, method = "GET", body = null) {
    const url = "http://localhost/api/v1" + path;

    const options = {
        method,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'X-Requested-With': 'XMLHttpRequest',
        },


    }

    if (body != null) {
        options.body = JSON.stringify(body);
    }

    return fetch(url, options)
}


export async function getAllCustomers() {

    try {
        const allCustomers = await api("/customer/getAllCustomers");



        return allCustomers.json();

    } catch (err) {

        console.log(err);
    }
}



export async function createCustomer(details) {

    try {
        const addCustomer = await api("/customer/addCustomer", "POST", details);
        //console.log(addCustomer.status)

        const data = await addCustomer.json();

        if(data.status=200){
            return {
                type:'success',
                desc:data.message
            }
        }else if(data.status=400){
            return {
                type:"error",
                desc:data.message
            }
        }else {
            throw Error("Server Error")
        }

    } catch(err){
        console.log("server error");
        return {
            type:'error',
            desc:"error"
       };
    }

    /*
    try{


        console.log(addCustomer)
        if(addCustomer.status==200){
            return {
                 type:'success',
                 desc:addCustomer.message 
            };
        }else  if(addCustomer.status=400){
            console.log(addCustomer)
            return {
                type:'error',
                desc:"User already exists.Please provide other details."
            };
        }else{
            
            throw Error("server error")
        }
        
    }catch(err){
        console.log(err)
        return {
            type:'error',
            desc:err.message + "asda"
       };
    }*/
}


/*
export async function addProduct(product){

    try{

        let response = await api("/product/addProduct","POST",product);


        if(response.status!==200){

            return "error";
        }else{

            return "succes";
        }
    }catch(err){

        return "error";
    }

   

    


    return response;
}

export async function deleteProduct(product){
    let body = product
    let response = await api(`/product/deleteProduct/?name=${product}`,"DELETE");
    //let data = await response.json();
   // return data;

}
*/

export default async function deleteEmail(email){
    try {
        const deleteUser = await api(`/customer/deleteCustomer/${email}`, "DELETE");

        const data = await deleteUser.json();

        if(data.status=200){
            return {
                type:'success',
                desc:data.message
            }
        }else if(data.status=400){
            return {
                type:"error",
                desc:data.message
            }
        }else {
            throw Error("Server Error")
        }

    } catch(err){
        console.log("server error");
        return {
            type:'error',
            desc:"error"
       };
    }
}