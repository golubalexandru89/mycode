import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';
import Header from './components/Header';
import Player from './components/Player';
import Input from './components/Input';

function App() {
  const [players,setPlayers] = useState([
    {
      id:1,
      score: 0,
      name:"Paul"
    },
    {
      id:2,
      score: 2,
      name:"Jhonny"
    },
    {
      id:3,
      score: 0,
      name:"Depp"
    },
    {
      id:4,
      score: 4,
      name:"Maria"
    },
    {
      id:5,
      score: 0,
      name:"Arthur"
    }
  ])
  const [playersLength, setPlayersLength] = useState(players.length)
  
  useEffect(()=> {
    setPlayersLength(playersLength);
  },[players,playersLength])


  function addPlayer(data){
    console.log(data)
    players.push(data)
    setPlayersLength(players.length)
  //  console.log(players)
  }
  function removePlayer(data){
    console.log(data);
    let remainingPlayers = players.filter((x) => x !== data)
    setPlayers(remainingPlayers)
  }
  return (
    <div className="App">
     <Header players={players} />

     {players.length>0&&(
      players.map(player => {
        return <Player player={player} key={player.id} actions={removePlayer}/>
      })
     )}

     <Input newPlayer={addPlayer} players={playersLength}/>
    </div>
  );
}

export default App;
