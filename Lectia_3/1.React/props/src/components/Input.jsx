import { useState } from "react";

function Input({newPlayer, players}) {

    //console.log(players);

    const [name, setName] = useState("");

    function addingPlayer(e) {
        if (name === ''){return}
        let thePlayer = {id:players+1,score:0,name:name}
        newPlayer(thePlayer)
    }

    function addName(e) {
        setName(e.target.value)
    }
    return (
        <>
            <div className="app-footer">
                <div className="input">
                    
                    <input type="text" className="player" placeholder="ENTER A PLAYER NAME" onChange={addName}></input>
                    <button className="btn playerAdd" onClick={addingPlayer}>Add player</button>
                    
                </div>
            </div>
        </>
    )
}

export default Input;
