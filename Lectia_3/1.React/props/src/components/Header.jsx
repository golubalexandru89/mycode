function Header({players}) {
   

    function totalPoints(){
        let points = 0;
        for (let i = 0; i<players.length; i++) {
         points = points + players[i].score;
            
        }

        return points;
    }



    return (
        <header>
            <button>test</button>
            <div className="score">
                <p>Total players:{players.length}</p>
                <p>Total points {totalPoints()}</p>
            </div>
            <h1>SCOREBOARD</h1>
            <div className="stopwatch">
                <h1>STOPWACH</h1>
                <p>0</p>
                <div className="buttons"><button className="btn start">Start</button><button className="btn reset">Reset</button>
                </div>
            </div>
        </header>
    )
}

export default Header;
