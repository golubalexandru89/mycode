import { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


function Player({player, actions}) {

    const [user,setUser] = useState(player);

 const test = () => {
    //console.log(user)
    actions(user)
    //actions(e.target)
 }

     useEffect(()=>{



        //console.log(player)

     },[])
   
    return (
        <main>
            <div className="playerBox">
                <div className="player"><button onClick={test}>Remove</button>
                    <p>{player.name}</p>
                </div>
                <div className="counter">
                    <button className="gray reduce">-</button>
                    <p>{player.score}</p>
                    <button className="gray add">+</button>
                </div>
            </div>
        </main>
    )
}

export default Player;
