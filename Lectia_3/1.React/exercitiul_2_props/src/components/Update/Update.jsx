import { useState } from "react";
import { addProduct, deleteProduct } from "../../service/api";
import AlertMessage from "../Alert/Alert";

function Update({product, changePage}) {
        

    let [title,setTitle] = useState(product.name);
    let [price,setPrice] = useState(product.price);
    let [stock,setStock] = useState(product.stock);

    let [error,setError] = useState([])

    let addTitle = (e) => {
        setTitle(e.target.value)
    }
    let addPrice = (e) => {
        setPrice(e.target.value)
    }

    let addStock =(e)=>{
        setStock(e.target.value);
    }

    async function update(){
        let product = {"name":title,"price":+price,"stock":+stock};
        let deleteItem = await deleteProduct(product.name)
        let rez = await addProduct(product)
        changePage("logo")
        
    }

    function cancel(){
        changePage("logo")
    }
    async function deleteProd(){
       let deleteItem = await deleteProduct(product.name);
       if(deleteItem.status){
        changePage('logo')
       } else if(deleteItem.status == false){
        setError(deleteItem.message)
        console.log("FALSE")
       }
    }
    return (
        <>
        <div className="update-form">
            <h1>Update Product</h1>
            <form>
                <p>
                    <label htmlFor="productTitle"> Product Title</label><br></br>
                    <input name="productTitle" type="text" id="productTitle" defaultValue={product.name} onChange={addTitle}></input>
                </p>
                <p>
                    <label htmlFor="productPrice"> Product Price</label><br></br>
                    <input name="productPrice" type="integer" id="productPrice" defaultValue={product.price} onChange={addPrice}></input>
                </p>
                <p>
                    <label htmlFor="productStock"> Product Stock</label><br></br>
                    <input name="productStock" type="integer" id="productStock" defaultValue={product.stock} onChange={addStock}></input>
                </p>

               
            </form>
            <div>
            <button onClick={update} className="btn btn-primary">Update</button>
            <button onClick={cancel} className="btn ">Cancel</button>
            <button onClick={deleteProd} className="btn btn-danger">Delete</button>
            </div>
            </div>

            {error.length !== 0 && (
                  <AlertMessage type='error' message={error}></AlertMessage>
            )}
        </>
    )
}

export default Update;