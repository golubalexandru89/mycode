import { useState } from "react";
import { addProduct } from "../../service/api";
import AlertMessage from "../Alert/Alert";


export const INITIAL = "initial";

export const LOADING = "loading";

export const SUCCESS = 'success';

export const FAIL = 'fail';

function Add({changePage}) {

    //console.log(changePage)
    const [productName, setName] = useState('')
    const [productPrice, setPrice] = useState(0);
    const [productStock, setStock] = useState(0);

    const [errors, setErrors] = useState([]);
    const [apiResponse, setApiResponse] = useState();

    const [requestAddedState, setRequestAddedState] = useState(INITIAL);

    let cancelPage = () => {
        changePage('logo')
    }

    let handleErrors = () => {

        let aux = [];

        if (productName == '') {

            aux.push("Please add a product name");
        }

        if (productPrice == '') {
            aux.push("Please add a price")
        }

        if (productStock == '') {
            aux.push("Please add the quantity")
        }

        setErrors(aux);


    }

    let createProduct = async () => {
        handleErrors();
        
        
        
        if (errors.length === 0) {

            let product = { "name": productName, "price": +productPrice, "stock": +productStock }
            
            setRequestAddedState(LOADING);
            let rez = await addProduct(product);
            //console.log(rez.status)


            if (rez.status) {
                setRequestAddedState(SUCCESS);
                setApiResponse(rez.message)
                changePage('logo')
            } else {
                setRequestAddedState(FAIL)
                setApiResponse(rez.message)
            }

        }

    }

    let addName = (e) => {
        setName(e.target.value)
    }

    let addPrice = (e) => {
        setPrice(e.target.value)
    }



    let addStock = (e) => {
        setStock(e.target.value)
    }

    return (
        <>
            <div className="add-content container">

                <fieldset>
                    <legend>ADD A PRODUCT</legend>

                    <div className="form-group">
                        <label htmlFor="product_name" className="form-label mt-4">Product Name</label>
                        <input onChange={addName} type="productName" className="form-control" id="product_name" aria-describedby="emailHelp" placeholder="Enter product name"></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="product_price" className="form-label mt-4">Product Price</label>
                        <input onChange={addPrice} type="productPrice" className="form-control" id="product_price" placeholder="Enter product price" ></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="product_stock" className="form-label mt-4">Product Stock</label>
                        <input onChange={addStock} type="productStock" className="form-control" id="product_stock" placeholder="Enter product price" ></input>
                    </div>
                    <br></br>
                    <button onClick={createProduct} className="btn btn-primary">Submit</button>

                </fieldset>
                <br></br>
                    <button onClick={cancelPage} className="btn ">Cancel</button>
            </div>

            {errors.length !== 0 && (errors.map((error) => {
               return  <AlertMessage type='error' message={error}></AlertMessage>
            }
            ))}


            {
                requestAddedState == FAIL && (<AlertMessage type="error" message={apiResponse} />)
            }

            {
                requestAddedState == SUCCESS && (<AlertMessage type="success" message={apiResponse} />)
            }
        </>
    )
}

export default Add;