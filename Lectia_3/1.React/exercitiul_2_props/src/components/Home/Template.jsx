import { useEffect, useState } from "react";
import Card from "./Card";
import { getProducts } from '../../service/api'
import './../../App.css';
import Home from './Home';
import Add from './../Add/Add';
import Update from "../Update/Update";


function Template({pageIs,changePage}) {
    let [updateProduct, setUpdateProduct] = useState();
    
    let returnUpdateProduct = (x)=>{
        setUpdateProduct(x)
    }


    
    return (
        <>
            {
                pageIs == "logo" && (<Home changePage={changePage} updateCard={setUpdateProduct}/>)
            }

            {
                pageIs == "add" && (<Add changePage={changePage}></Add>)
            }
            {pageIs == 'update' && (<Update product={updateProduct} changePage={changePage}></Update>)}
        </>
    )
}
export default Template;