function Card({product, changePage, updateCard}) {



    const handleClick = (e) => {
       // console.log(product)
        updateCard(product)
        changePage('update')
    }
    return (<>
    <div onClick={handleClick}>
        <div className="card mb-3">
            
            <svg xmlns="http://www.w3.org/2000/svg" className="d-block user-select-none" width="100%" height="200" aria-label="Placeholder: Image cap" focusable="false" role="img" preserveAspectRatio="xMidYMid slice" viewBox="0 0 318 180">
                <rect width="100%" height="100%" fill="#868e96"></rect>
                <text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
            </svg>
            <div className="card-body">
                <h5 className="card-title">{product.name}</h5>
                <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
            
            <div className="card-body justify-between">
                <p href="#" className="card-link">${product.price}</p>
                <p href="#" className="card-link">Units left: {product.stock}</p>
            </div>
            
        </div>
        
        </div>
    </>
    )
}

export default Card;