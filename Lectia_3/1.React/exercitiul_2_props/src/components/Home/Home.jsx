import { useEffect, useState } from "react";
import Card from "./Card";
import { getProducts } from '../../service/api'
function Home({changePage,updateCard}) {


    const [products, setProducts] = useState(["abc", "asd", "fqw"]);
    //stari ->loading  succes fail

    const handleProducts = async () => {
        try {
            const data = await getProducts();
            setProducts(data);
        } catch (error) {
            console.error("Error fetching products:", error);
        }
    };

    useEffect(() => {
        handleProducts();
    }, [])

    let test = () => {
        console.log("hey")
    }
    return (
        <>


            <div className="grid">
                {products.length > 0 && (
                    products.map(product => {
                        return <Card key="{product.id}" product={product} changePage={changePage} updateCard={updateCard}></Card>
                    })

                )}
            </div>
        </>
    )
}

export default Home;