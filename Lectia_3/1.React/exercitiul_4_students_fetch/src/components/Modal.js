function Modal(props) {
    const user = props.item;
    return (
        <>
        
            <li className="student-item cf">
                <div className="student-details" onClick={()=>{props.createModal(props.id)}}>
                    <img className="avatar" src={user.picture.medium} alt="Profile Picture" />
                    {props.id===props.editId ? (
                         <h3>
                            <input value={props.editFName}onChange={(e)=>{props.setEditFName(e.target.value)}}></input>
                            <input value={props.editLName}onChange={(e)=>{props.setEditLName(e.target.value)}}></input>
                         </h3>)
                    :(
                    <h3>{user.name.first} {user.name.last}</h3>)}
                    
                    {props.id===props.editId ? (
                        <span className="email"><input value={props.editEmail} onChange={(e)=>{props.setEditEmail(e.target.value)}}></input></span>
                    ):(
                    <span className="email">{user.email}</span>)}
                </div>
                <div className="joined-details">
                    <span className="date">{user.registered.date}</span>
                </div>
                <div className="config-buttons">
                    {props.id===props.editId ? (
                            <button className="edit" onClick={() => props.handleSave(props.id)}>Save</button>
                    ) : (
                        <button className="edit" onClick={()=>{ props.handleEdit(props.id,user.name.first,user.name.last, user.email)}}>Edit</button>
                        )}
                    
                    <button className="remove"
                        onClick={() => {
                            props.handleRemove(props.id)
                        }}>
                        Remove
                    </button>
                </div>
            </li>
        </>
    )
}

export default Modal;