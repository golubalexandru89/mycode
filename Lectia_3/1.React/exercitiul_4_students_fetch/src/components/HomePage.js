import { useEffect, useState } from "react";
import Card from "./Card";
import { getUsers } from "../api/services";

import { ArrowLeftIcon } from "@heroicons/react/24/solid";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


function Homepage() {
    const [data, setData] = useState([])
    const [currentPage, setCurrentPage] = useState(1);
    const [maxPages, setMaxPages] = useState([])
    const [editId, setEditId] = useState(-1)
    const [editFName, setEditFName] = useState('')
    const [editLName, setEditLName] = useState('')
    const [editEmail, setEditEmail] = useState('')
    const [searchString, setSearchString] = useState('')
    const [searchStudents, setSearchStudents] = useState([])
    const [openModal, setOpenModal] = useState(false)
    const [userModal, setUserModal] = useState()
    const [currentModalId, setCurrentModalId] = useState();


    useEffect(() => {
        async function fetchData() {
            try {
                let value = await getUsers();
                let response = value.results;
                setData(response)
            } catch (error) {
                console.log(error)
            }
        }
        function calculatePages() {
            const max = Math.ceil(data.length / 9)
            const rez = []
            for (let i = 1; i < max; i++) {
                rez.push(i)
            }
            setMaxPages(rez)

        }

        fetchData();
        calculatePages()
    }
        , [])

    useEffect(() => {
        // Now 'data' contains the fetched results
        // You can perform additional actions here
    }, [data, currentPage, maxPages, editId, searchStudents]);


    function handleRemove(id) {
        const newList = [...data];
        newList.splice(id, 1)
        setData(newList)
    }

    function handleEdit(id, fname, lname, email) {
        setEditFName(fname)
        setEditLName(lname)
        setEditEmail(email)
        setEditId(id)

    }

    function handleSave(id) {
        setEditId(id + 1)
        const newList = [...data]
        newList[editId + 1].name.first = editFName
        newList[editId + 1].name.last = editLName
        newList[editId + 1].email = editEmail
        setEditId('')
        setData(newList)

    }

    function pagination(page, arr) {
        let rez = [];
        for (let i = page * 9 - 9 + 1; i < arr.length && i <= page * 9 - 1; i++) {
            rez.push(arr[i])
        }
        return rez;
    }

    let paginatedData = pagination(currentPage, data)

    useEffect(() => {

    }, [editId, paginatedData, searchString])

    function searchStudent(arr, text) {
        let res = [];
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].name.first.toLowerCase().includes(text.toLowerCase())
                || arr[i].name.last.toLowerCase().includes(text.toLowerCase())) {
                res.push(arr[i])
            }
        }
        return res;
    }


    function createModal(id) {
        setCurrentModalId(id)
        setUserModal(data[id+1])
        console.log(userModal)
        setOpenModal(true)
        console.log(data[id+1])
    }

    return (

        <div>
            {openModal ? (<><div className="page">
                <div className="card-details">
                    <img src={userModal.picture.thumbnail} alt="Profile Picture" className="student-image" /><br></br>
                    <h3>{userModal.name.first} {userModal.name.last}</h3>
                    <span>Username</span><br></br>
                    <span>{userModal.email}</span><br></br>
                    <span>{userModal.location.city}</span><br></br>
                    <span>{userModal.phone}</span><br></br>
                    <span>{userModal.location.street.name}</span><br></br>
                    <span>Birthday: {userModal.registered.date}</span>
                </div>
                <div className="icon icon-left">
                <a className="left" onClick={()=>{setUserModal(data[currentModalId])}}>Left</a>
                </div>

                <div className="icon icon-right">
                    <a className="right" onClick={()=>{setUserModal(data[currentModalId+2])}}>Right</a>
                </div>

                <div className="icon icon-close">
                    <a className="close-modal" onClick={()=>{setOpenModal(false)}}>Close</a>
                </div>
            </div>
            </>) : (<div className="page">

                <header className="header">
                    <h2>Students</h2>


                    <label htmlFor="search" className="student-search">
                        <input id="search" placeholder="Search by name..." value={searchString}

                            onChange={(e) => {
                                setSearchString(e.target.value);
                                let result = searchStudent(data, searchString);
                                setSearchStudents(result)
                            }} />
                        <button type="button" className="student-search-button"><img src="icn-search.svg" alt="Search icon" /></button>
                    </label>



                </header>

                <ul className="student-list">
                    {searchStudents.length > 0 ?
                        (
                            searchStudents.map((item, id) => {
                                return (<Card
                                    item={item}
                                    key={id}
                                    id={id}
                                    handleRemove={handleRemove}
                                    handleEdit={handleEdit}
                                    handleSave={handleSave}
                                    editId={editId}
                                    editFName={editFName}
                                    editLName={editLName}
                                    editEmail={editEmail}
                                    setEditFName={setEditFName}
                                    setEditLName={setEditLName}
                                    setEditEmail={setEditEmail}
                                    createModal={createModal}

                                > </Card>
                                )
                            })
                        ) : (

                            paginatedData.map((item, id) => {
                                return (<Card
                                    item={item}
                                    key={id}
                                    id={id}
                                    handleRemove={handleRemove}
                                    handleEdit={handleEdit}
                                    handleSave={handleSave}
                                    editId={editId}
                                    editFName={editFName}
                                    editLName={editLName}
                                    editEmail={editEmail}
                                    setEditFName={setEditFName}
                                    setEditLName={setEditLName}
                                    setEditEmail={setEditEmail}
                                    createModal={createModal}

                                > </Card>)
                            }))}




                </ul>

                <div className="pagination">
                    <ul className="link-list">
                        <li>
                            <button type="button" className="active" onClick={() => {
                                setCurrentPage(currentPage - 1)
                            }}>Prev</button>
                        </li>
                        {maxPages.map((item) => {
                            return <li key={item}>
                                <button type='button' className="active" onClick={() => {
                                    if (currentPage <= 1) {
                                        setCurrentPage(maxPages)
                                    }
                                    setCurrentPage(item)
                                }} >{item}</button>
                            </li>
                        })}
                        <li>
                            <button type="button" className="active" onClick={() => {
                                if (currentPage >= maxPages.length) {

                                    setCurrentPage(1)
                                }
                                setCurrentPage(currentPage + 1)
                            }}>Next</button>
                        </li>

                    </ul>
                </div>
            </div>
            )}
        </div>
    )
}

export default Homepage;