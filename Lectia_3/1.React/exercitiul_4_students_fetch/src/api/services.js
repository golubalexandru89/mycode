function api(path, method = "GET", body = null) {
    const url = "https://randomuser.me/api/" + path;

    const options = {
        method,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'X-Requested-With': 'XMLHttpRequest',
        },


    }

    if (body != null) {
        options.body = JSON.stringify(body);
    }

    return fetch(url, options)
}

export async function getUsers() {
    try{
        const users = await api("?results=100");
       // console.log("api worked")
        return users.json()
    }catch(error){
        console.log(error)
    }
}