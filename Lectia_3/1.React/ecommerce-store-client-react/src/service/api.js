export function api(path, method = "GET", body = null) {
    const url = "http://localhost/api/v1" + path;

    const options = {
        method,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'X-Requested-With': 'XMLHttpRequest'
        }
    }

    if (body != null) {
        options.body = JSON.stringify(body);
    }
    return fetch(url, options)
};

export async function getAllProducts() {
    try {
        let response = await api("/product/getAllProducts");
        //console.log(data)
        return response.json();
    } catch (err) {
        console.log(err)
    }
};

export async function addProduct(data) {
    try {
        let response = await api("/product/addProduct","POST",data);
        //console.log(data)
        return response;
    } catch (err) {
        console.log(err)
    }
};

 