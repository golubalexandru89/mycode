
import './App.css';
import NavigationBar from './components/Navbar';

import Home from './components/Home';
import AddProductPage from "./components/AddProduct"
import { Route, Routes, BrowserRouter } from 'react-router-dom';

function App() {



  return (
    <>

      <BrowserRouter>
        <NavigationBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/add-product" element={<AddProductPage />} />
        </Routes>
      </BrowserRouter>




    </>

  );
}

export default App;
