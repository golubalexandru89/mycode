import { useEffect, useState } from "react";
import { getAllProducts } from "../service/api";
import ItemCard from "./ItemCard";



function Home() {

    let [products, setProducts] = useState([]);

    let fetchProducts = async () => {
        let data = await getAllProducts();
        console.log(data)
        setProducts(data);
    }

    useEffect(() => {
        fetchProducts();
    }, [])
    return (
        <main className="grid-container">
            {products.length > 0 && (
                products.map(item => {
                    return <ItemCard item={item} key={item.name} />
                })
            )
            }

        </main>
    );
}

export default Home;