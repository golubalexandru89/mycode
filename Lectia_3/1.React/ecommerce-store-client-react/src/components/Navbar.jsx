import { useNavigate } from "react-router-dom";

function NavigationBar() {
    let navigate = useNavigate();

    function addProduct() {
        navigate("/add-product")
    }

    return (
        
        <header>
            
            <button className="btn button btn-add" onClick={addProduct}> Add PRODUCT</button>
            <button className="btn button btn-atc"> Spre COS</button>
        </header>

    )
}
export default NavigationBar;