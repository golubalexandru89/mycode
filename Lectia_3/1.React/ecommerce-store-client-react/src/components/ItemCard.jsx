function ItemCard({item}){
    return (
        <div class="grid-item">
                <div class="product_img"><img
                        src="https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg" />
                </div>
                <div class="product_details">
                    <div class="product_details_info">
                        <div class="product_details_title">{item.name}</div>
                        <div class="product_details_description">Carry All The Things with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.</div>
                    </div>
                    <div class="product_details_action_bar">
                        <div class="product_details_pricing">${item.price}</div>
                        <button> Add to Cart</button>
                    </div>
                </div>
            </div>
    )
}

export default ItemCard;


