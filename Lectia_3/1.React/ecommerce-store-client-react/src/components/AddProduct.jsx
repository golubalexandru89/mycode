import { useState } from "react";
import { api } from "../service/api"
import { useNavigate } from "react-router-dom";

function AddProductPage() {

    let navigate = useNavigate();

    function cancelProduct() {
        navigate("/")
    }

    async function addProduct(product) {
        try{
            let response = await api('/product/addProduct',"POST",product)
            return response.json();
        }catch(err){
            console.log(err)
        }
    }

    let [name,setName]=useState('');
    let [price,setPrice]=useState(0);
    let [stock,setStock]=useState(0);

    const addChanges = (e) =>{
        if(e.target.name === "product-name"){
            setName(e.target.value)
        }else if (e.target.name === 'product-price'){
            setPrice(e.target.value)
        }else if (e.target.name === 'product-stock'){
            setStock(e.target.value)
        }
        
    }

    const addProductFunction = () => {
        let product = {name, price:+price,stock:+stock}
        console.log(product)
        addProduct(product)

    }
    return(
        <>
        <div className="form">
        <input type="text" name="product-name" placeholder="Product Name" onChange={addChanges}/>
        <input type="number" name="product-price" placeholder="Product Price" onChange={addChanges}/>
        <input type="number" name="product-stock" placeholder="Stock" onChange={addChanges}/>
        <button className="btn button add-new-product" onClick={addProductFunction}>Add Product</button>
        <button className="btn button cancel-product" onClick={cancelProduct}>Cancel</button>
        </div>
        </>
    )
}//crud ->create read update delete

export default AddProductPage;