let submit = document.querySelector(".submit");
let person = document.querySelector(".person");
let card = document.querySelector("#invitedList");
let confirmed = document.querySelector(".confirmed");



let cards=[];

function createItem(content) {

    let cardToAdd = document.createElement("li");
    let span = document.createElement("span");
    span.textContent = content;
    let label = document.createElement("label");
    label.textContent = "Confirmed";
    let input = document.createElement("input");
    input.type = "checkbox";

    input.classList.add("conf");
    let buttonEdit = document.createElement("button");
    buttonEdit.textContent = "edit";
    let buttonRemove = document.createElement("button");
    buttonRemove.textContent = "remove";

    label.appendChild(input);
    cardToAdd.appendChild(span);
    cardToAdd.appendChild(label);
    cardToAdd.appendChild(buttonEdit);
    cardToAdd.appendChild(buttonRemove);

    return cardToAdd;

}





submit.addEventListener("click", (e) => {
    //console.log(person.value);
    //console.log(card.firstElementChild);
    
    let newCard = createItem(person.value);
    
    card.appendChild(newCard);
    person.value = ""
})


card.addEventListener("click", (e) => {
    
    let obj = e.target;
    
    if (obj.tagName === "BUTTON") {
        
        if (obj.textContent === "edit") {
            //console.log("Pressed Edit Button")
            let card = obj.parentNode;
            let title = card.firstElementChild;
            let titleInput = title.textContent;
            title.textContent = "";
            console.log(titleInput)
            obj.textContent = "save"
            let newInput = document.createElement("input");
            title.appendChild(newInput);
            newInput.value = titleInput;
            
            
            
            
            
            
            
            
        } else if (obj.textContent === "remove") {
            
            let parent = obj.parentNode;
            let card = parent.parentNode;
            card.removeChild(parent)
            
        } else if (obj.textContent === "save") {
            let card = obj.parentNode;
            let title = card.firstElementChild;
            let titleValue = title.firstElementChild.value;
            title.removeChild(title.firstElementChild)
            title.textContent = titleValue;
            obj.textContent = "edit";
        }
        
    }
    
    
})



function showConfirmed() {

    let childrens = card.children;
    //console.log(childrens)
    let arr = Array.from(childrens);


    cards=[...arr];


    for(let i=0;i<arr.length;i++){


        let chk=arr[i].querySelector(".conf");

       // console.log(chk.checked);

        if(chk.checked==false){

            arr[i].style.display='none';
        }
    }
    
    //console.log(arr.querySelector(".conf"));
    return arr;

}




function allCards() {

   
    
    let arr = [...cards];

    for(let i=0;i<arr.length;i++){


        let chk=arr[i].querySelector(".conf");

        //console.log(chk.checked);

        arr[i].style.display='block';
        
    }
    
    //console.log(arr.querySelector(".conf"));
    return arr;

}



confirmed.addEventListener("click", (e) => {
    if (confirmed.checked == true) {
       showConfirmed();
    }
    if (confirmed.checked == false) {
       allCards();
    }    
})