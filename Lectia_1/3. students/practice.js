function pagination(pagina,arr){
    let rez = [];
    for (let i = pagina*9-9; i<arr.length&&i<=pagina*9-1;i++){
        rez.push(arr[i])
    }
    return rez;
}


function createCard(persoana){
    let li = document.createElement("li");
    li.classList.add("student-item");
    li.classList.add("cf");

    li.innerHTML=`
    <div class="student-details">
        <img class="avatar" src="${persoana.picture.thumbnail}" alt="Profile Picture">
        <h3>${persoana.name.first} ${persoana.name.last}</h3>
        <span class="email">${persoana.email}</span>
        </div>
    <div class="joined-details">
        <span class="date">${persoana.registered.date}</span>
    </div>
    <div class="config-buttons">
        <button class="edit">Edit</button>
        <button class="remove">Remove</button>
    </div>

    `
    return li;
}

function populateCards(arr){
    let studentParent = document.querySelector(".student-list");

    studentParent.innerHTML="";
    for (let i = 0; i<arr.length; 1++){
        studentParent.appendChild(createCard(arr[i]))
    }
}





populatePag(1,data);



