populatePag(1,data);
addPagination(data);



// Page Navigation
let page = document.querySelector('.link-list');
page.addEventListener('click', (e)=>{
    let navBtn = document.querySelector('.active')
    navBtn.classList.remove('active')
    e.target.classList.add('active')
    populatePag(e.target.textContent,data)
})
// Search 
let search = document.querySelector('.student-search-button');
search.addEventListener('click',(e)=>{

    let query = document.querySelector('#search')
    console.log(query.value);
    if(query.value==""){
        populatePag(1,data)
        addPagination(data)
    }else{
    showResults(data,query.value);
   
}
   // searchStudent(data,query.value);
})



let configButtons = document.querySelector('.student-list');
configButtons.addEventListener('click',(e)=>{
    
    let cardDetails = e.target.parentNode.parentNode;
    let personDetails = cardDetails.firstElementChild.firstElementChild;
    let joinDate = cardDetails.children[1].children[0]
    let personName = personDetails.nextElementSibling;
    let personEmail = personName.nextElementSibling;
    /*console.log("cardDetails")
    console.log(cardDetails.children[1].children[0])
    console.log("personDetails")
    console.log(personDetails)
    console.log("personName")
    console.log(personName)
    console.log("personEmail")
    console.log(personEmail)*/

    if(e.target.textContent=="Remove"){
      
        //todo: firstName si lastName in consola
       let card= e.target.parentNode.parentNode;
       //console.log(card);
       let studentDetails = card.firstElementChild.children[1].textContent;
       //console.log(studentDetails);
       let studentName = splitName(studentDetails);

       data=[...eraseStudent(data,studentName[0],studentName[1])];



       populatePag(1,data);



    } 
    

    else if(e.target.textContent=="Edit"){
        e.target.textContent="Save"
        let oldDate = joinDate.textContent.replace("");
        console.log(oldDate);
        joinDate.textContent="";
        joinDate.appendChild(createInput(oldDate));


        let personEmailValue = personEmail.textContent;
        let inputfield2 = document.createElement("input");
        inputfield2.classList ="new-email";
        inputfield2.value=personEmailValue;
        personEmail.textContent="";
        personEmail.appendChild(inputfield2)
        /*
        let personFullName=personName.textContent;
        //console.log(defaultName)
        let personData = [personFullName,personEmailValue]
        
        let inputfield1 = document.createElement("input");
        inputfield1.value=personFullName;
        personName.textContent="";
        personName.appendChild(inputfield1);
        
        
        personEmail.appendChild(inputfield2);
        */

        //return console.log(personData)
    


       // console.log(personEmailValue)
    } 
    
    
    else if(e.target.textContent=="Save"){
        e.target.textContent="Edit"

        let stName = splitName(personName.textContent);
        let st={

            first:stName[0],
            last:stName[1],
            date:joinDate.textContent,
            email:personEmail.firstElementChild.value
        }
       
        editStudent(data,st);
       
       let saveEmail = document.querySelector(".new-email");
       let saveDate = document.querySelector(".new-date");
       let saveEmailValue = saveEmail.value;
       let saveDateValue = saveDate.value;
       saveEmail.parentNode.textContent = saveEmailValue;
       saveDate.parentNode.textContent = saveDateValue;

      
    }


})


let studentCards = document.querySelectorAll(".student-item");
    for (let i = 0; i <studentCards.length;i++){
        studentCards[i].addEventListener("click",(e)=>{
            let sName = studentCards[i].children[0].children[1].textContent
            let split = splitName(sName);
            let studentss = findStudentByFistLastName(data,split[0],split[1]);
            
        handleModal(studentss)
        })


      
}


