function  createCard(persoana){


    let li=document.createElement("li");
    li.classList.add("student-item")
    li.classList.add("cf");


    li.innerHTML=`
    <div class="student-details">
        <img class="avatar" src="${persoana.picture.thumbnail}" alt="Profile Picture">
        <h3>${persoana.name.first} ${persoana.name.last}</h3>
        <span class="email">${persoana.email}</span>
    </div>
    <div class="joined-details">
        <span class="date">${persoana.registered.date}</span>
    </div>
    <div class="config-buttons">
        <button class="edit">Edit</button>
        <button class="remove">Remove</button>
    </div
    
    `

return li;


}

function populateCards(arr){
    let studentParent = document.querySelector(".student-list");

    studentParent.innerHTML='';
    for (let i = 0; i<arr.length;i++){
        studentParent.appendChild(createCard(arr[i]));
    }
}


//functie ce primeste ca parametru pagina si arr => returneaza 
//un vector cu 9 elemnete corespunzatoare paginii data

//ia data.length si vezi daca sunt mai mult de 9
// daca sunt mai putin de noua, populate card
// daca sunt mai mult de noua, 
// add html la link list

function pagination(pagina,arr){
    let rez=[];
    for(let i=pagina*9-9;i<arr.length&&i<=pagina*9-1;i++){
        rez.push(arr[i]);
    }
    return rez;

}


function populatePag(pag,arr) {
    populateCards(pagination(pag,arr));
}


function addPagination(arr){
    let ul = document.querySelector(".link-list");

    ul.innerHTML=`<li>
    <button type="button" class="active">1</button>
  </li>`;
    for (i=2; i<arr.length/9+1;i++){
        let button = document.createElement("button");
        button.type = "button";
        button.textContent= i;
        let li = document.createElement("li")
        li.appendChild(button);
        ul.appendChild(li);
    }
}

function searchStudent(arr,text){

    
    let  res=[];


    for(let i=0;i<arr.length;i++){

        if(arr[i].name.first.toLowerCase().includes(text.toLowerCase())
        ||arr[i].name.last.toLowerCase().includes(text.toLowerCase())){


            res.push(arr[i]);

        }
    }

    return res;
}


function showResults(arr,value){
    populateCards(searchStudent(arr,value))
    addPagination(searchStudent(arr,value));
}

function eraseStudent(arr,firstName,lastName){
    //console.log(firstName+" "+lastName)
    let  aux=arr.filter((e)=>e.name.first.toLowerCase()!==firstName.toLowerCase()&&e.name.last.toLowerCase()!==lastName.toLowerCase())
    
    return aux;
}

function splitName(arr){
    let splitName = arr.split(" ");
    //console.log(splitName)
    return splitName;
}

function createInput(arr){
    let input = document.createElement("input")
    input.setAttribute("type","date");
    input.classList = "new-date";
    input.value=arr;
    return input;
}

//mailul data si first si last

function findStudentByFistLastName(arr,first,last){

    let aux = arr.filter((e)=>e.name.first.toLowerCase()==first.toLowerCase()&&e.name.last.toLowerCase()==last.toLowerCase())
    console.log(aux)
    return aux[0];    //todo functie ce returneaza studentul cu atributele date

}


// first last  email date  

// 

function editStudent(arr,student){
    console.log(arr);
    console.log(student);
    let editableStudent=findStudentByFistLastName(arr,student.first,student.last);
    console.log(editableStudent)
    if(student.date!==editableStudent.registered.date){
        editableStudent.registered.data=student.date;
    }
    if(student.email!==editableStudent.email){
       editableStudent.email=student.email;
    }
    let aux=[...eraseStudent(arr,student.first,student.last)];
    aux.push(editableStudent);
    console.log(aux)
    return aux;
}



//function that creates modal card

function createModalCard(student){
    let card = document.createElement("section")
    card.classList.add("card-modal")
    card.innerHTML = `
    <div class="card-details">
      <img src="${student.picture.thumbnail}" alt="Profile Picture" class="student-image">
      <h3>${student.name.first} ${student.name.last}</h3>
      <span>Username</span>
      <span>${student.email}</span>
      <span>tallahasse</span>
      <span>2291404900</span>
      <span>4035 fairview str</span>
      <span>Birthday: ${student.registered.date}</span>
    </div>
      <div class="icon icon-left">
       <a class="left"> <i class="fa-solid fa-angle-left"></i></a>
      </div>
      
      <div class="icon icon-right">
        <a class="right"><i class="fa-solid fa-angle-right"></i></a>
      </div>
      
      <div class="icon icon-close">
        <a  class="close-modal"><i class="fa-solid fa-xmark"></i></a>
      </div>`


      return card;


}
//todo:function with student 

function handleModal(student){

    let modal = document.querySelector(".modal");
    modal.innerHTML="";
    modal.appendChild(createModalCard(student));
    modal.classList.toggle("hide");
    
    let closeModal = document.querySelector(".close-modal");
    
    closeModal.addEventListener("click",(e)=>{
        modal.classList.toggle("hide");
    })

    let left = document.querySelector(".left")
    left.addEventListener("click",(e)=>{
        let name = splitName(e.target.parentNode.parentNode.parentNode.children[0].children[1].textContent)
        let prev = returnLateral(data,name[0],name[1])
        console.log(prev)
        modal.classList.toggle('hide')
        if(prev[0]==undefined){
            handleModal(data[data.length-1])
        }else{
            handleModal(prev[0])
        }
    })

    let right = document.querySelector(".right")
    right.addEventListener("click",(e)=>{
        let name = splitName(e.target.parentNode.parentNode.parentNode.children[0].children[1].textContent)
        let next = returnLateral(data,name[0],name[1])
        console.log(next)
        modal.classList.toggle('hide')

        if(next[1]==undefined){
            handleModal(data[0])
        }else{
            handleModal(next[1])
        }
    })
    
}



/*
let closeModal = document.querySelector(".close-modal");
closeModal.addEventListener("click",(e)=>{
    console.log("Close")
    //handleModal(e);
})*/

function returnLateral(arr,firstN,lastN){
    let people = [];

    for (let i = 0; i<arr.length;i++){
        
        if(arr[i].name.first.toLowerCase()==firstN.toLowerCase()&&arr[i].name.last.toLowerCase()==lastN.toLowerCase()){
            people.push(arr[i-1]);
            people.push(arr[i+1]);
        }
    }

    return people;
}