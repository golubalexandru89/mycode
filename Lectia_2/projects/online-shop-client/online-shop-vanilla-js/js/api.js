function api(path, method = "GET", body = null) {
    const url = "http://localhost/api/v1" + path;

    const options = {
        method,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'X-Requested-With': 'XMLHttpRequest'
        }
    }

    if (body != null) {
        options.body = JSON.stringify(body);
    }
    return fetch(url, options)
}


export async  function getAllProducts(){
    let response=  await api("/product/getAllProducts");
    let  data= await response.json();
    //console.log(data)
    return data;
}

export async function addProduct(product){

    try{

        let response = await api("/product/addProduct","POST",product);


        if(response.status!==200){

            return "error";
        }else{

            return "succes";
        }
    }catch(err){

        return "error";
    }

   

    


    return response;
}

export async function deleteProduct(product){
    let body = product
    let response = await api(`/product/deleteProduct/?name=${product}`,"DELETE");
    //let data = await response.json();
   // return data;

}

export async function addRandomProduct(product){}

export async function getProductbyName(name){
    let response = await api(`/product/getProductByName/?name=${name}`);
    let data = await response.json();
   // console.log(data)
    return data
}


export async function update(name,product){
    await api(`/product/deleteProduct/?name=${name}`,"DELETE");
    return addProduct(product);
}