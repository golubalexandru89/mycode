import { deleteProduct, getAllProducts, getProductbyName, update } from "./api.js";

import { addProduct } from "./api.js";







let cos = [];


export function setHome() {




    let container = document.querySelector(".container");



    container.innerHTML = `
    
    
    
    
    <header>

    <button class="btn button btn-add"> Add PRODUCT</button>
    <button class="btn button btn-atc"> Spre COS</button>
        </header>

        <main class="grid-container">
            <div class="grid-item">
                <div class="product_img"><img
                        src="https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg">
                </div>
                <div class="product_details">
                    <div class="product_details_info">
                        <div class="product_details_title">Sauce Labs Backpack</div>
                        <div class="product_details_description">carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.</div>
                    </div>
                    <div class="product_details_action_bar">
                        <div class="product_details_pricing">$29</div>
                        <button> Add to Cart</button>
                    </div>
                </div>
            </div>
            <div class="grid-item">
                <div class="product_img"><img
                        src="https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg">
                </div>
                <div class="product_details">
                    <div class="product_details_info">
                        <div class="product_details_title">Sauce Labs Backpack</div>
                        <div class="product_details_description">carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.</div>
                    </div>
                    <div class="product_details_action_bar">
                        <div class="product_details_pricing">$29</div>
                        <button> Add to Cart</button>
                    </div>
                </div>
            </div>
            <div class="grid-item">
                <div class="product_img"><img
                        src="https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg">
                </div>
                <div class="product_details">
                    <div class="product_details_info">
                        <div class="product_details_title">Sauce Labs Backpack</div>
                        <div class="product_details_description">carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.</div>
                    </div>
                    <div class="product_details_action_bar">
                        <div class="product_details_pricing">$29</div>
                        <button> Add to Cart</button>
                    </div>
                </div>
            </div>
            <div class="grid-item">
                <div class="product_img"><img
                        src="https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg">
                </div>
                <div class="product_details">
                    <div class="product_details_info">
                        <div class="product_details_title">Sauce Labs Backpack</div>
                        <div class="product_details_description">carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.</div>
                    </div>
                    <div class="product_details_action_bar">
                        <div class="product_details_pricing">$29</div>
                        <button> Add to Cart</button>
                    </div>
                </div>
            </div>
            <div class="grid-item">
                <div class="product_img"><img
                        src="https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg">
                </div>
                <div class="product_details">
                    <div class="product_details_info">
                        <div class="product_details_title">Sauce Labs Backpack</div>
                        <div class="product_details_description">carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.</div>
                    </div>
                    <div class="product_details_action_bar">
                        <div class="product_details_pricing">$29</div>
                        <button> Add to Cart</button>
                    </div>
                </div>
            </div>
            
        </main>


         <footer>

         </footer>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    `



    attachCards();

    let checkoutPage = document.querySelector('.btn-atc');

    checkoutPage.addEventListener('click', (e) => {
        setCos();
    })

    let btnAdd = document.querySelector('.btn-add');

    btnAdd.addEventListener("click", (e) => {

        setAddNewProductpage();
    })




    let cardsContainer = document.querySelector(".grid-container");

    let oldProduct = {};
    cardsContainer.addEventListener("click", async (e) => {



        let sender = e.target;
        if (sender.classList.contains("update-product")) {
            sender.classList.add('save-product')
            sender.classList.remove('update-product');

            let name = getProductNameFromBtn(sender);

            let product = await getProductbyName(name);

            oldProduct = product;

            let senderParent = sender.parentNode.parentNode;

            replaceIncardWithInputs(senderParent, product);

            sender.textContent = "Save"


        } else if (sender.classList.contains("delete-product")) {
            let name = getProductNameFromBtn(sender);
            deleteProduct(name);
            let deletedItem = sender.parentNode.parentNode.parentNode;
            deletedItem.remove();


        } else if (sender.classList.contains('save-product')) {
            let name = getProductNameFromBtn(sender)
            let price = getProductPriceFromBtn(sender)
            let product = { "name": name, "price": Number(price), "stock": 5 }
            await update(oldProduct.name, product)
            await attachCards();

        } else if (sender.classList.contains('btn-add-card')) {
            let prodName = await getProductNameFromBtn(sender);
            updateCos({ name: prodName, qty: 1 });

        }






    });
}

function createProduct(product) {
    let element = document.createElement('div');
    element.classList.add("grid-item");
    element.innerHTML = `<div class="product_img"><img src="https://www.saucedemo.com/static/media/sauce-backpack-1200x1500.0a0b85a3.jpg">
                        </div>
                        <div class="product_details">
                        <div class="product_details_info">
                            <div class="product_details_title">${product.name}</div>
                            <div class="product_details_description">carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.</div>
                        </div>
                        <div class="product_details_action_bar">
                            <div class="product_details_pricing">$${product.price}</div>
                            <button class="btn btn-add-card">Add to Cart</button> 
                            <button class="btn update-product">Update</button>
                            <button class="btn delete-product">Delete</button>
                            
                            `

    return element;
}

function getProductNameFromBtn(button) {

    let productTitle = button.parentNode.parentNode.children[0].children[0].textContent;
    if (productTitle == '') {
        productTitle = button.parentNode.parentNode.children[0].children[0].children[0].value;
    }

    return productTitle;
}

function getProductPriceFromBtn(button) {
    let productPrice = button.parentNode.parentNode.children[1].children[0].children[0].value;
    return productPrice
}

function replaceIncardWithInputs(card, product) {

    let inptName = document.createElement("input");
    inptName.value = product.name;
    card.querySelector('.product_details_title').textContent = ''
    card.querySelector('.product_details_title').prepend(inptName);

    let inptPricing = document.createElement("input");
    inptPricing.value = product.price;
    card.querySelector('.product_details_pricing').textContent = ''
    card.querySelector(".product_details_pricing").prepend(inptPricing);
}

async function attachCards() {

    let gridContainer = document.querySelector(".grid-container");

    gridContainer.innerHTML = "<p>Loading......................</p>";



    let products = await getAllProducts();



    gridContainer.innerHTML = "";
    products.forEach(element => {

        gridContainer.appendChild(createProduct(element));



    }
    );




}

function setAddNewProductpage() {
    let container = document.querySelector(".container");
    container.innerHTML = `
    <div class="form">
        <input type="text" name="product-name" placeholder="Product Name">
        <input type="number" name="product-price" placeholder="Product Price">
        <input type="number" name="product-stock" placeholder="Stock">
        <button class="btn button add-new-product">Add Product</button>
        <button class="btn button cancel-product">Cancel</button>
    </div>
    `

    let cancel = document.querySelector(".cancel-product");

    cancel.addEventListener('click', (e) => {

        setHome();
    })

    let addProd = document.querySelector(".add-new-product");

    addProd.addEventListener('click', async (e) => {
        let parent = e.target.parentNode;
        let productToAdd = { "name": parent.children[0].value, "price": +parent.children[1].value, "stock": +parent.children[2].value }
        let resp = await addProduct(productToAdd)

        if (resp !== "error") {

            setHome();
        } else {

            alert("!!!!!");
        }
    })



}

function updateCos(cosItem) {
    let poz = findCosItemPositionByName(cosItem.name);
    if (poz != -1) {
        cos[poz].qty += cosItem.qty;
    } else {
        cos.push(cosItem);
    }
}

function findCosItemPositionByName(name) {
    for (let i = 0; i < cos.length; i++) {
        if (cos[i].name == name) {
            return i;
        }

    }
    return -1;

}
export async function setCos() {
    let container = document.querySelector(".container");

    container.innerHTML = `
    
    <header>
            
    <button class="btn button btn-add"> Add PRODUCT</button>
    <button class="btn button btn-home"> Spre Home</button>

</header>

<main class="grid gap-4">
   
    <div class="atc-product-list">
        
       
    </div>
    
</main>


 <footer>

 </footer>
        
    `


    for (let i = 0; i < cos.length; i++) {
        let response = await getProductbyName(cos[i].name);
        addToCartProducts(response.name, cos[i].qty, response.price)

    }

    let remove = document.querySelectorAll('.atc-remove')
    for (let i = 0; i < remove.length; i++) {
        remove[i].addEventListener('click', (e) => {
            let it = e.target.parentNode.parentNode
            let prodName = it.children[1].children[0].textContent
            removeFromCart(prodName)
        })
    }


    let spreHome = document.querySelector('.btn-home');

    spreHome.addEventListener('click', (e) => {
        setHome();
    })



}

function removeFromCart(name) {
            for (let i = 0; i<cos.length;i++){
            if (cos[i].name == name) {
               cos.splice(i,1)
            }
        }
        setCos()
  
}

function addToCartProducts(name, qty, price) {

    let atcProducts = document.querySelector('.atc-product-list');

    let atcItem = document.createElement("div");
    atcItem.classList.add('atc-product-item');

    atcItem.innerHTML = `
    <div class="atc-items">
        <input value="${qty}" />
    </div>
    <div class="atc-item-details">
        <div class="atc-item-title">${name}</div>
        <div class="atc-item-description">
            A red light isn't the desired state in testing but it sure helps when riding your bike at night. Water-resistant with 3 lighting modes, 1 AAA battery included.
        </div>
        <div class="atc-item-pricing">
            $${price}
        </div>
    </div>
    <div class="atc-item-buttons">
        <button class="btn button atc-remove">Remove</button>
    </div>
    
    
    
    `
    atcProducts.appendChild(atcItem)
}
