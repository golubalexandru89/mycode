let x = 3;

let a;



function ex(nr) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            if (nr > 10) {


                x = x + nr;

                res("succes");
            } else {

                rej("fail");
            }

        }, 1000)

    });
}


ex(15)
    .then(data => {

        console.log(data);
        console.log(x);
        a = x;

        return a;
    })
    .then(data => {

        console.log(a);
    })
    .catch(err => {

        console.log(err);

    })