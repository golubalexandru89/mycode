fetch("https://dog.ceo/api/breeds/list/all")
.then(response=>{
   
    return response.json();
})
.then((data)=>{
    
   return data.message;

})
.then((data)=>{

    let option = document.querySelector('#breeds');
    option.innerHTML = "";
    for(let key in data){
        let newOption = document.createElement('option')
        newOption.innerText = key.toUpperCase();
        option.appendChild(newOption);
        console.log(key);
    }
})
.catch(err=>{


    console.log(err);
})


let selectBreed = document.querySelector("#breeds")
selectBreed.addEventListener('change',(e)=>{
    //console.log(selectBreed.value);
    fetch(`https://dog.ceo/api/breed/${selectBreed.value.toLowerCase()}/images/random`)
    .then(response=>{
        return response.json()
    })
    .then(response => {
        return response.message
    })
    .then(response => {
         let img = document.querySelector('.dog-img');
         //console.log(img)
         img.src = response;

         console.log(response)
    }).catch(err=>{
        console.log(err)
    })

})

let newImage = document.querySelector(".see-more");
newImage.addEventListener('click',(e)=>{
    let actualDog = document.querySelector('#breeds').value.toLowerCase();
    
    fetch(`https://dog.ceo/api/breed/${actualDog}/images/random`)
.then(response=>{
    return response.json()
})
.then(response => {
    return response.message
})
.then(response =>{
    let img = document.querySelector('.dog-img');
         //console.log(img)
         img.src = response;
})

})